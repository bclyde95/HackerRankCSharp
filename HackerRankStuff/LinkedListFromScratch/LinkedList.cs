﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRankStuff
{
    class LinkedList
    {
        //Props
        Node<int> head;
        int count;
        //Constructors
        public LinkedList()
        {
            head = null;
            count = 0;
        }

        public LinkedList(Node<int> newHead)
        {
            head = newHead;
            count = 1;
        }
        //Methods
        //add
        public void add (int newData)
        {
            Node<int> temp = new Node<int>(newData);
            Node<int> current = head;
            while (current.GetNext() != null)
            {
                current = current.GetNext();
            }
            current.SetNext(temp);
            count++;
        }
        //get
        public int get(int index)
        {
            if (index <= 0)
            {
                return -1;
            }
            Node<int> current = head;
            for (int i = 1; i < index; i++)
                current = current.GetNext();
            return current.GetData();
        }
        //size
        public int size() => count;
        //isEmpty
        public bool isEmpty()
        {
            if (head == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //remove
        public void Remove()
        {
            Node<int> current = head;
            while (current.GetNext().GetNext() != null)
            {
                current = current.GetNext();
            }
            current.SetNext(null);
            count--;
        }

        //static void Main(string[] args)
        //{
        //        string s = "cindy";
        //        int.Parse(s);
        //}
    }

    
}
