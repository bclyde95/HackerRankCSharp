﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRankStuff
{
    class Node<Data>
    {
        Node<Data> next;
        Data data;

        public Node (Data newData)
        {
            data = newData;
            next = null;
        }

        public Node (Data newData, Node<Data> newNext)
        {
            data = newData;
            next = newNext;
        }

        public Data GetData() => data;

        public Node<Data> GetNext() => next;

        public void SetData(Data newData)
        {
            data = newData;
        }

        public void SetNext (Node<Data> newNode)
        {
            next = newNode;
        }
    }
}
