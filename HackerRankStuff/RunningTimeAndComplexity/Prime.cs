﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRankStuff.RunningTimeAndComplexity
{
    class Prime
    {

        static long LongParse()
        {
            string input = Console.ReadLine();
            long v;
            if (long.TryParse(input, out v))
                return v;
            else
            {
                Console.WriteLine("Enter an integer");
                return LongParse();
            }
        }

        static bool IsPrime(long num)
        {
            long sqrt = (long)Math.Sqrt(num);
            if (sqrt % 2 == 0)
                return false;
            if (num == 1)
                return false;
            if ((sqrt / 2) % 2 == 0)
                return false;
            for (long i = 2, j = sqrt; i < j; i++, j--)
            {
                if (num % i == 0)
                    return false;
                
                if (num % j == 0)
                    return false;
            }
            return true;
        }

        static bool IsPrimeLinear(long num)
        {
            if (num == 1)
                return false;
            if (Math.Sqrt(num) / 2 % 2 == 0)
                return false;
            for (long i = 2; i <= (long)Math.Sqrt(num); i++)
            {
                if (num % i == 0)
                    return false;
            }
            return true;
        }

        static void PrintPrime(bool p)
        {
            if (p)
                Console.WriteLine("Prime");
            else
                Console.WriteLine("Not prime");
        }

        //static void Main(String[] args)
        //{
        //    /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution */
        //    long numOfInput = LongParse();
        //    long startTime;
        //    long endTime;
        //    long duration;
            

        //    for (int i = 0; i < numOfInput; i++)
        //    {
        //        long input = LongParse();
        //        startTime = DateTime.Now.Millisecond;
        //        PrintPrime(IsPrime(input));
        //        endTime = DateTime.Now.Millisecond;
        //        duration = endTime - startTime;
        //        Console.WriteLine("Duration is {0}", duration);

        //        startTime = DateTime.Now.Millisecond;
        //        PrintPrime(IsPrimeLinear(input));
        //        endTime = DateTime.Now.Millisecond;
        //        duration = endTime - startTime;
        //        Console.WriteLine("Duration is {0}", duration);
        //    }
        //}
    }
}
