﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRankStuff.RunningTimeAndComplexity
{
    class RuntimePractice
    {
        static int findNumberOfRepsLinear(string s, char c)
        {
            //Linear-O(n)
            int sum = 0;//O(1)-Constant

            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] == c)
                {
                    sum++;
                }
            }
            return sum;
        }

        static int findNumberOfReps(string s, char c)
        {
            //Linear-O(n)
            int sum = 0;//O(1)-Constant
            int mid = s.Length / 2;
            for (int i = 0, j = s.Length - 1; i < j; i++, j--) // O(1)  O(n/2)
            {
                if (s[i] == c) //O(n)
                    sum++;
                if (s[j] == c) //O(n)
                    sum++;
            }
            
            return sum;
        }

        static int[] findNumsOfRepsV1(string s, char[] c)
        {
        //Quadratic time - O(n*n)
            int[] sums = new int[c.Length]; //O(1)

            for (int i = 0; i < s.Length; i++) //O(1), O(n+1), O(n)
            {
                for (int j = 0; j < c.Length; j++) //O(n), O(n*m + 1), O(n*m) 
                {
                    if (s[i] == c[j])//O(n*m)
                        sums[j]++;
                }
            }
            return sums; //O(1)
        }

        static int[] findNumOfRepsV2 (string s, char[] c)
        {
            //Optimal time O(n)
            int[] sums = new int[c.Length];
            Hashtable map = new Hashtable();
            for (int i = 0; i< s.Length; i++)
            {
                if (!map.ContainsKey(s[i]))
                {
                    map.Add(s[i], i);
                }
               else
                {
                    int sum = (int)map[s[i]];
                    map[s[i]] = sum + 1;
                }
            }

            for (int i = 0; i < c.Length; i++)
            {
                int sum;
                if (!map.ContainsKey(c[i]))
                {
                    sums[i] = 0;
                }
                else
                {
                    sums[i] = (int)map[c[i]];
                }
            }
            return sums;
        }

        //static void Main (string[] args)
        //{
        //    string s = "anjnvsdonajnsdjnansndjnaljangsjnfogaosfingasodngaosnogasidnofksdofksdfogndfoneoinwodknlkgashdfjsanjsncvjanxjcanjsdnkfjanoqwjndfoajsdnofnflaksndfooqwekfnsaldknfoqkwndlfkandsokfqnwdofaksdnfalskcnvknlknalskdnkofaokenfgkjroieroikndfkasjdofiwernlkeojidknvoiernksodifansdofiowerniodufoaisdasdfnasodnfosaindofsaidnvosicvbaosidfjaosidvoiasudoriwheroidhfisaoncviasdofiasjdfoiroisajdfoiasodifasjodncoivnaosidnofiashdogiasdgoisdovsnoaidshfasiodhfoeruiodhfosadifoasdnoidjasoidofwqkneroiusdjofnasdoicjasodifnaosirjoasdnfoiasdjrandoif";
        //    char[] a = new char[] { 'a', 'c', 'e', 'g' };
        //    Console.WriteLine(s.Length);


        //    long startTime = DateTime.Now.Millisecond;
        //    Console.WriteLine(findNumberOfRepsLinear(s, 'a'));
        //    long endTime = DateTime.Now.Millisecond;
        //    long duration = endTime - startTime;
        //    Console.WriteLine("Duration is {0}", duration);

        //    startTime = DateTime.Now.Millisecond;
        //    Console.WriteLine(findNumberOfReps(s, 'a'));
        //    endTime = DateTime.Now.Millisecond;
        //    duration = endTime - startTime;
        //    Console.WriteLine("Duration is {0}", duration);

        //    startTime = DateTime.Now.Millisecond;
        //    findNumsOfRepsV1(s, a);
        //    endTime = DateTime.Now.Millisecond;
        //    duration = endTime - startTime;
        //    Console.WriteLine("Duration is {0}", duration);

        //    startTime = DateTime.Now.Millisecond;
        //    findNumOfRepsV2(s, a);
        //    endTime = DateTime.Now.Millisecond;
        //    duration = endTime - startTime;
        //    Console.WriteLine("Duration is {0}", duration);
        //    Console.ReadLine();
        //}
    }
}
