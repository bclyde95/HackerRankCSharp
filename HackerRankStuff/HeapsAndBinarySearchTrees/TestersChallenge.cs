﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRankStuff.HeapsAndBinarySearchTrees
{
    class TestersChallenge
    {
        public static Random rand = new Random();

        static int RandInt(int min, int max)
        {
            return rand.Next((max-min) + 1) + min;
        }

        static void PrintInput(int lectures, int students, int cancelCutoff, int[] arrivals)
        {
            Console.WriteLine(lectures);
            Console.WriteLine("{0} {1}", students, cancelCutoff);
            foreach (int i in arrivals)
                Console.Write(i + " ");
            Console.WriteLine();
            Console.WriteLine();
        }

        static List<int> usedStudentNums = new List<int>();

        static int StudentNumValidate(int i)
        {
            if (usedStudentNums.Contains(i))
                return StudentNumValidate(i + 1);
            usedStudentNums.Add(i);
            return i;

        }

        //static void Main(string[] args)
        //{
        //    int count = 0;
            
        //    while (count < 5)
        //    {
        //        int numOfLectures = RandInt(1, 5);
        //        int numOfStudents = StudentNumValidate(RandInt(3, 200));
        //        int cancelNum = RandInt(1, numOfStudents);
        //        int[] arrivalTimes = new int[numOfStudents];
        //        int arrivalLow = (int)Math.Pow(-10, 2);
        //        int arrivalHigh = (int)Math.Pow(10, 2);
        //        for (int i = 0; i < arrivalTimes.Length; i++)
        //        {
        //            arrivalTimes[i] = RandInt(arrivalLow, arrivalHigh);
        //        }
        //        if (!arrivalTimes.Contains(0))
        //        {
        //            arrivalTimes[RandInt(0, numOfStudents - 1)] = 0;
        //        }
        //        count++;
        //        PrintInput(numOfLectures, numOfStudents, cancelNum, arrivalTimes);
        //    }
        //}
    }
}
