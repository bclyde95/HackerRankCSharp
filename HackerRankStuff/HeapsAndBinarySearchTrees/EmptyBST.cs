﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRankStuff.HeapsAndBinarySearchTrees
{
    class EmptyBST<D> : Tree<D> where D : IComparable
    {
        public EmptyBST()
        {

        }

        public bool IsEmpty() => true;

        public int Cardinality() => 0;

        public bool Member(D elt) => false;

        public NonEmptyBST<D> Add(D elt) => new NonEmptyBST<D>(elt);
    }
}
