﻿using System;
using System.Collections.Generic;

namespace HackerRankStuff.HeapsAndBinarySearchTrees
{
    public class NonEmptyBST<D> : Tree<D> where D : IComparable
    {
        D data;
        Tree<D> left;
        Tree<D> right;

        public NonEmptyBST(D elt)
        {
            data = elt;
            left = new EmptyBST<D>();
            right = new EmptyBST<D>();

        }

        public NonEmptyBST(D elt, Tree<D> leftTree, Tree<D> rightTree)
        {
            data = elt;
            left = leftTree;
            right = rightTree;
        }

        public bool IsEmpty() => false;

        //Data exists so add one. If data exists in left or right section of tree respectively, 
        //each will return 0 or 1 and the cardinality sum for the tree is returned 
        public int Cardinality() => 1 + left.Cardinality() + right.Cardinality();

        public bool Member(D elt)
        {
            if (data.Equals(elt))
            {
                return true;
            }
            else
            {
                if(elt.CompareTo(data) < 0)
                {
                    return left.Member(elt);
                }
                else
                {
                    return right.Member(elt);
                }
            }
        }

        public NonEmptyBST<D> Add (D elt)
        {
            if (data.Equals(elt))
            {
                return this;
            }
            else
            {
                if (elt.CompareTo(data) < 0)
                {
                    return new NonEmptyBST<D>(data, left.Add(elt), right);
                }
                else
                {
                    return new NonEmptyBST<D>(data, left, right.Add(elt));
                }
            }
        }
    }
}