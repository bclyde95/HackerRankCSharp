﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.Collections;

namespace HackerRankStuff.HeapsAndBinarySearchTrees
{
    class Testers
    {
        

        //Random Ints
        public static int RandInt (int min, int max)
        {
            Random rand = new Random();
            return rand.Next((max - min) + 1) + min;
        }

        //Random BST
        public static Tree<int> RandTree(int count)
        {
            if (count == 0)
                return new EmptyBST<int>();
            else
                return RandTree(count - 1).Add(RandInt(0, 50));
        }

        public static void CheckIsEmpty(Tree<int> t)
        {
            //If the Tree 't' is an instance of EmptyBST ==> IsEmpty(t) == true
            //If the Tree 't' is an instance of NonEmptyBST ==> IsEmpty(t) == false
            if (t.GetType() == typeof(EmptyBST<int>))
            {
                if (t.IsEmpty())
                    Console.WriteLine("PASS: The tree is an EmptyBST");
                else
                    throw new Exception("FAIL: IsEmpty() is not working properly. This instance was empty");
            }
            else if (t.GetType() == typeof(NonEmptyBST<int>))
            {
                if (!t.IsEmpty())
                    Console.WriteLine("The tree is a NonEmptyBST");
                else
                    throw new Exception("FAIL: IsEmpty() is not working properly. This instance was non-empty");
            }
        }

        public static void CheckAddMemberCardinality(Tree<int> t, int x)
        {
            int nt = t.Add(x).Cardinality();
            //Case 1: variable was added and Cardinality increase by 1.
            if (nt == t.Cardinality() + 1)
            {
                if (t.Member(x))
                    throw new Exception("The cardinality was increased but the var was already a member of the tree");
            }
            /*Case 2: variable already existed in tree and therefore 
             * was not really added, Cardinality did not change*/
            else if (nt == t.Cardinality())
            {
                if (t.Member(x))
                    throw new Exception("The cardinality did not increase when the var was added");
            }
            else
            {

                throw new Exception("Check methods or tests for functionality");
            }

        }
    }
}
