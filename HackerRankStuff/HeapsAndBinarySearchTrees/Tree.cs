﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRankStuff.HeapsAndBinarySearchTrees
{
    public interface Tree<D> where D : IComparable
    {
        bool IsEmpty();
        int Cardinality();
        bool Member(D elt);
        NonEmptyBST<D> Add(D elt);
    }
}
