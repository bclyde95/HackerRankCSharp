﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace HackerRankStuff.RegexAndDatabases
{
    class MatchingChallenge
    {
        static string NameParser(string name)
        {
            if (name.Length <= 20 && name.Length > 0)
            {
                if (Regex.Match(name, "^[a-z]+$").Success)
                    return name;
                else
                    throw new Exception("Names can only contain letters");
            }
            else
            {
                if (name.Length > 20)
                    throw new Exception("Name cannot be over 20 characters long");
                if (name.Length == 0)
                    throw new Exception("Name must have a value, please enter your name");
            }
            return "0";
        }

        static string EmailParser(string email)
        {
            if (email.Length <= 50 && email.Length > 0)
            {
                if (Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
                    return email;
                else
                    throw new Exception("Please enter a valid email");
            }
            else
            {
                if (email.Length > 50)
                    throw new Exception("Email cannot be over 50 characters");
                if (email.Length == 0)
                    throw new Exception("Email must have a value, please enter an email");
            }
            return "0";
        }

        static List<string> NameSort(List<KeyValuePair<string,string>> s)
        {
            var res = from c in s
                      where c.Value.EndsWith("@gmail.com")
                      orderby c.Key ascending
                      select c.Key;
            return res.ToList();
        }

        //static void Main(String[] args)
        //{
        //    int N = Convert.ToInt32(Console.ReadLine());
        //    List<KeyValuePair<string,string>> names = new List<KeyValuePair<string,string>>();
        //    for (int a0 = 0; a0 < N; a0++)
        //    {
        //        string[] tokens_firstName = Console.ReadLine().Split(' ');
        //        string firstName = NameParser(tokens_firstName[0]);
        //        string emailID = EmailParser(tokens_firstName[1]);
        //        KeyValuePair<string, string> pair = new KeyValuePair<string, string>(firstName, emailID);
        //        names.Add(pair);
        //    }
        //    List<string> output = NameSort(names);
        //    foreach (string i in output)
        //        Console.WriteLine(i);
        //}
    }
}
