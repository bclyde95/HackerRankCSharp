﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRankStuff.BSTTicTacToe
{
    class Logic
    {
        #region Class Variables

        public char[] board;
        public char userMarker;
        public char aiMarker;
        public char winner;
        public char currentMarker;
        
        #endregion

        //Constructor
        public Logic(char playerToken, char aiToken)
        {
            userMarker = playerToken;
            aiMarker = aiToken;
            winner = '-';
            this.board = SetBoard();
            this.currentMarker = userMarker;
        }

        #region Methods checking validity of moves

        public bool WithinRange(int num) => num > 0 && num < board.Length - 1;

        public bool IsSpotTaken(int num) => board[num - 1] != '-';

        #endregion

        #region Methods to render board to console

        public void PrintBoard()
        {
            Console.WriteLine();
            for (int i = 0; i < board.Length; i++)
            {
                if (i % 3 == 0 && i != 0)
                {
                    Console.WriteLine();
                    Console.WriteLine("---------------");
                }
                Console.Write(" | " + board[i]);
            }
        }

        public static void PrintIndexBoard()
        {
            Console.WriteLine();
            for (int i = 0; i < 9; i++)
            {
                if (i % 3 == 0 && i != 0)
                {
                    Console.WriteLine();
                    Console.WriteLine("---------------");
                }
                Console.Write(" | " + (i+1));
            }
        }

        #endregion

        #region GamePlay

        public static char[] SetBoard()
        {
            char[] board = new char[9];
            for (int i = 0; i < board.Length; i++)
            {
                board[i] = '-';
            }
            return board;
        }

        public bool PlayTurn(int spot)
        {
            bool isValid = WithinRange(spot) && IsSpotTaken(spot);
            if (isValid)
            {
                board[spot - 1] = currentMarker;
                currentMarker = currentMarker == userMarker ? aiMarker : userMarker;
            }
            return isValid;
        }

        public bool IsThereAWinner()
        {
            bool diagsAndMids = (RightDi() || LeftDi() || MiddleRow() || SecondCol()) && board[4] != '-';
            bool topsAndFirsts = (TopRow() || FirstCol()) && board[0] != '-';
            bool bottomAndThird = (BottomRow() || ThirdCol()) && board[8] != '-';
            if (diagsAndMids)
            {
                this.winner = board[4];
            }
            else if (topsAndFirsts)
            {
                this.winner = board[0];
            }
            else if (bottomAndThird)
            {
                this.winner = board[8];
            }

            return diagsAndMids || topsAndFirsts || bottomAndThird;
        }

        #region IsThereAWinner Helper Methods

        public bool RightDi() => board[0] == board[4] && board[4] == board[8];

        public bool LeftDi() => board[2] == board[4] && board[4] == board[6];

        public bool TopRow() => board[0] == board[1] && board[1] == board[2];

        public bool MiddleRow() => board[3] == board[4] && board[4] == board[5];

        public bool BottomRow() => board[6] == board[7] && board[7] == board[8];

        public bool FirstCol() => board[0] == board[3] && board[3] == board[6];

        public bool SecondCol() => board[1] == board[4] && board[4] == board[7];

        public bool ThirdCol() => board[2] == board[5] && board[5] == board[8];

        #endregion

        public bool IsBoardFilled()
        {
            for (int i = 0; i < board.Length; i++)
            {
                if (board[i] == '-')
                    return false;
                
            }
            return true;
        }

        public string GameOver()
        {
            if (IsThereAWinner())
                return string.Format("Game Over! {0} is the winner", this.winner);
            else if (IsBoardFilled())
                return "Draw! Game Over!";
            else
                return "Not over!";
        }

        #endregion

        #region Input Parsing Methods

        public int IntParseAsync()
        {
            char c = Console.ReadKey().KeyChar;
            int v;
            if (int.TryParse(c.ToString(), out v))
                return v;
            else
            {
                Console.WriteLine("Please enter an integer");
                return IntParseAsync();
            }
        }

        #endregion
    }
}
