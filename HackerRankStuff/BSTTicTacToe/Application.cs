﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRankStuff.BSTTicTacToe
{
    class Application
    {
        static char charParse(string s)
        {
            char r;

            if (char.TryParse(s, out r))
            {
                return r;
            }
            else
            {
                Console.WriteLine("Input was not a char type");
                return charParse(s);
            }
        }


        //static void Main(string[] args)
        //{
        //    CharScanner sc = new CharScanner();

        //    //Allows for continuous games
        //    bool doYouWantToPlay = true;
        //    while (doYouWantToPlay)
        //    {
        //        Console.WriteLine("Welcome to Tic Tack Toe. Prepare to face the master!");
        //        Console.WriteLine("Are You Ready?");
        //        Console.WriteLine("Wait! But first you must pick characters for yourself and I.");

        //        Console.Write("Enter a single character that will represent you: ");
        //        char playerToken = Console.ReadKey().KeyChar;

        //        Console.WriteLine();

        //        Console.Write("Enter a single character that will represent me: ");
        //        char aiToken = Console.ReadKey().KeyChar;

        //        Console.WriteLine();

        //        Logic game = new Logic(playerToken, aiToken);
        //        AI ai = new AI();

        //        Console.WriteLine();

        //        //Setup the game
        //        Console.WriteLine("Now we can begin.");
        //        Console.WriteLine("To play, enter a number and your token shall be put in it's place.");
        //        Console.WriteLine("The number go from 1-9, left to right. We shall see who will win this round");
        //        Logic.PrintIndexBoard();
        //        Console.WriteLine();

        //        Console.WriteLine("Let's start our game.");

        //        //GamePlay
        //        while (game.GameOver() == "Not over!")
        //        {
        //            if (game.currentMarker == game.userMarker)
        //            {
        //                //user turn
        //                Console.WriteLine("It's your turn! Enter a spot for your token: ");
        //                int spot = game.IntParseAsync();
        //                while (!game.PlayTurn(spot))
        //                {
        //                    Console.WriteLine("Spot {0} is not available", spot);
        //                    spot = game.IntParseAsync();
        //                }
        //                Console.WriteLine("You picked {0}", spot);
        //            }
        //            else
        //            {
        //                //AI turn
        //                Console.WriteLine("It's my turn!");
        //                //Pick spot
        //                int aiSpot = ai.pickSpot(game);
        //                //play turn
        //                game.PlayTurn(aiSpot);
        //                Console.WriteLine("I picked {0}!", aiSpot);
        //            }

        //            Console.WriteLine();

        //            //Print current board
        //            game.PrintBoard();
        //            Console.WriteLine();
        //        }

        //        Console.WriteLine(game.GameOver());
        //        Console.WriteLine();
        //        Console.WriteLine("Do you want to play again? Enter 'Y' for Yes or press any other key to exit");
        //        char response = char.ToUpper(Console.ReadKey().KeyChar);
        //        doYouWantToPlay = (response == 'Y');
        //        Console.WriteLine();
        //        Console.WriteLine();
        //        Console.WriteLine();
        //    }
        //}
    }
}
