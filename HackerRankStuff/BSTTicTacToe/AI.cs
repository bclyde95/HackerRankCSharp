﻿using System;
using System.IO;
using System.Collections.Generic;

namespace HackerRankStuff.BSTTicTacToe
{
    class AI
    {
        public AI()
        {

        }

        public int pickSpot(Logic game)
        {
            List<int> choices = new List<int>();

            for (int i = 0; i < 9; i++)
            {
                //if slot is open add as choice
                if (game.board[i] == ' ')
                {
                    choices.Add(i + 1);
                }
            }

            Random rand = new Random();
            int choice = choices[Math.Abs(rand.Next() % choices.Count)];
            return choice;
        }
    }
}