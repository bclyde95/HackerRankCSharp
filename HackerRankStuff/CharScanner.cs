﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HackerRankStuff
{
    class CharScanner
    {
        public CharScanner()
        {
            Read();
        }

        char currentChar;

        protected void Read()
        {
            if (Console.KeyAvailable)
            {
                ConsoleKeyInfo key = Console.ReadKey();
                currentChar = key.KeyChar;
            }
        }

        public string Next()
        {

            try
            {
                return char.ConvertFromUtf32(currentChar);
            }
            finally
            {
                Read();
            }
        }

        public bool hasNextInt()
        {
            if (char.ConvertFromUtf32(currentChar) == null)
                return false;
            int output;
            return int.TryParse(((char)currentChar).ToString(), out output);
        }

        public int nextInt()
        {
            try
            {
                return int.Parse(char.ConvertFromUtf32(currentChar));
            }
            finally
            {
                Read();
            }
        }

        public bool hasNextDouble()
        {
            if (currentChar != ' ')
                return false;
            double output;
            return double.TryParse(char.ConvertFromUtf32(currentChar), out output);
        }

        public double nextDouble()
        {
            try
            {
                return double.Parse(currentChar.ToString());
            }
            finally
            {
                Read();
            }
        }

        public bool hasNext() => currentChar != ' ';
    }
}
