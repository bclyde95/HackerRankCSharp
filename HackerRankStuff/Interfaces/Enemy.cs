﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRankStuff.Interfaces
{
    class Enemy : Character
    {
        public string Name { get; set; }

        public string Weapon { get; set; }

        public Enemy (string n, string w)
        {
            Name = n;
            Weapon = w;
        }

        public void WeaponDraw()
        {
            Console.WriteLine("{0} has drawn his {1}!", Name, Weapon);
        }

        public void Attack()
        {
            Console.WriteLine("{0} attacks you with his {1}!", Name, Weapon);
        }

        public void Heal()
        {
            Console.WriteLine("The Enemy heals himself!");
        }
    }
}
