﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRankStuff.Interfaces
{
    interface Character
    {
        void Attack();
        void Heal();
    }
}
