﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRankStuff.Interfaces
{
    class Hero : Character
    {
        public string Name { get; set; }

        public string Weapon { get; set; }

        public Hero(string n, string w)
        {
            Name = n;
            Weapon = w;
        }

        public void Attack()
        {
            Console.WriteLine("{0} attacks the enemy!", Name);
        }

        public void Heal()
        {
            Console.WriteLine("{0} heals you!",Name);
            
        }
    }
}
