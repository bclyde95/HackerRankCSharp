﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRankStuff.QueueAndStackFromScratch
{
    class Queuey<T>
    {
        LinkedList<T> queue;

        public Queuey()
        {
            queue = new LinkedList<T>();
        }

        public bool isEmpty() => queue.First == null;

        public int size() => queue.Count;

        public void enqueue (T n)
        {
            queue.AddLast(n);
        }

        public T dequeue()
        {
            T value = queue.First();
            queue.RemoveFirst();
            return value;
            
        }

        public T peek() => queue.First();
    }
}
