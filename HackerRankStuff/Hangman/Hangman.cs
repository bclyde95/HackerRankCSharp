﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Sockets;

namespace HackerRankStuff.Hangman
{
    class Hangman
    {
        #region Variables

        public string mysteryWord;
        StringBuilder currentGuess;
        List<char> previousGuesses = new List<char>();

        int maxTries = 6;
        int currentTry = 0;

        #endregion

        List<string> dictionary = new List<string>();
        private static StreamReader sr;
        private static StreamWriter sw;
        
        public Hangman()
        {
            InitializeStreams();
            mysteryWord = PickWord();
            currentGuess = InitializeCurrentGuess();
        }

        public void InitializeStreams()
        {
            //string[] lines = File.ReadAllLines("C:\\Users\\Owner\\documents\\visual studio 2015\\Projects\\HackerRankStuff\\HackerRankStuff\\Hangman\\wordsEn.txt");
            //foreach (string i in lines)
            //    dictionary.Add(i);

            try
            {
                FileStream inFile = new FileStream("C:\\Users\\Owner\\documents\\visual studio 2015\\Projects\\HackerRankStuff\\HackerRankStuff\\Hangman\\wordsEn.txt", FileMode.Open);
                sr = new StreamReader(inFile);
                sw = new StreamWriter(inFile);
                string currentLine = sr.ReadLine();
                while (currentLine != null)
                {
                    dictionary.Add(currentLine);
                    currentLine = sr.ReadLine();
                }
                sr.Close();
            }
            catch (IOException)
            {
                Console.WriteLine("Could not initialize streams");
            }
        }

        public string PickWord()
        {
            Random rand = new Random();
            int wordIndex = Math.Abs(rand.Next() % dictionary.Count());
            return dictionary[wordIndex];
        }

        public bool IsGuessedAlready() => previousGuesses.Contains(currentGuess.ToString().Last());

        public bool PlayGuess(char guess)
        {
            bool match = false;
            for (int i = 0; i < mysteryWord.Length; i++)
            {
                if (mysteryWord[i] == guess)
                {
                    currentGuess[i * 2] = guess;
                    match = true;
                    previousGuesses.Add(guess);
                }
            }
            if (!match)
                currentTry++;
            return match;
        }

        public StringBuilder InitializeCurrentGuess()
        {
            StringBuilder current = new StringBuilder();

            for (int i = 0; i < mysteryWord.Length * 2; i++)
            {
                if (i % 2 == 0)
                {
                    current.Append("_");
                }
                else
                {
                    current.Append(" ");
                }
            }

            return current;
        }

        public string GetCurrentGuess() => string.Format("Current guess: {0}", currentGuess.ToString());

        /* Draw
            " - - - - -\n"+
            "|        |\n"+
            "|        O\n"+
            "|       /|\\ \n"+
            "|        |\n"+
            "|       / \\ \n"+
            "| \n"+
            "| \n"; 
         */

        public string DrawPicture()
        {
            switch (currentTry)
            {
                case 0: return NoPersonDraw();
                case 1: return AddHeadDraw();
                case 2: return AddBodyDraw();
                case 3: return AddOneArmDraw();
                case 4: return AddSecondArmDraw();
                case 5: return AddFirstLegDraw();
                default: return FullBodyDraw();
            }
        }

        #region DrawPicture() Helper Methods

        private string FullBodyDraw()
        {
            return " - - - - -\n" +
            "|        |\n" +
            "|        O\n" +
            "|       /|\\ \n" +
            "|        |\n" +
            "|       / \\ \n" +
            "| \n" +
            "| \n";
        }

        private string AddFirstLegDraw()
        {
            return " - - - - -\n" +
            "|        |\n" +
            "|        O\n" +
            "|       /|\\ \n" +
            "|        |\n" +
            "|       / \n" +
            "| \n" +
            "| \n";
        }

        private string AddSecondArmDraw()
        {
            return " - - - - -\n" +
            "|        |\n" +
            "|        O\n" +
            "|       /|\\ \n" +
            "|        |\n" +
            "|        \n" +
            "| \n" +
            "| \n";
        }

        private string AddOneArmDraw()
        {
            return " - - - - -\n" +
            "|        |\n" +
            "|        O\n" +
            "|       /| \n" +
            "|        |\n" +
            "|         \n" +
            "| \n" +
            "| \n";
        }

        private string AddBodyDraw()
        {
            return " - - - - -\n" +
            "|        |\n" +
            "|        O\n" +
            "|        | \n" +
            "|        |\n" +
            "|         \n" +
            "| \n" +
            "| \n";
        }

        private string AddHeadDraw()
        {
            return " - - - - -\n" +
            "|        |\n" +
            "|        O\n" +
            "|        \n" +
            "|        \n" +
            "|        \n" +
            "| \n" +
            "| \n";
        }

        private string NoPersonDraw()
        {
            return " - - - - -\n" +
            "|        |\n" +
            "|        \n" +
            "|        \n" +
            "|        \n" +
            "|        \n" +
            "| \n" +
            "| \n";
        }

        #endregion

        public bool GameOver()
        {
            if (UserWin())
            {
                Console.WriteLine();
                Console.WriteLine("Congrats! You guessed {0}", mysteryWord);
                return true;
            }
            else if (UserLose())
            {
                Console.WriteLine();
                Console.WriteLine("You lost! You used all {0} of your tries.\nThe mystery word was {1}", maxTries, mysteryWord);
                return true;
            }

            return false;
        }

        #region GameOver() Helper Methods

        private bool UserWin()
        {
            string guess = GetCondensedCurrentGuess();
            return guess.Equals(mysteryWord);
        }

        private string GetCondensedCurrentGuess() => currentGuess.ToString().Replace(" ", "");

        private bool UserLose() => currentTry >= maxTries;

        #endregion
    }
}
