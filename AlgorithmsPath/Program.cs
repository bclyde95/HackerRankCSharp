﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorithmsPath
{
    class Program
    {
        static void Main(string[] args)
        {
            int numOfTrips = int.Parse(Console.ReadLine());
            int pooledDollars = int.Parse(Console.ReadLine());
            int numOfFlavors = int.Parse(Console.ReadLine());
            int key = 1;
            int[] temp = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            Dictionary<int, int> flavors = new Dictionary<int, int>(temp.Length);
            foreach (int i in temp)
            {
                flavors.Add(key, i);
                Console.WriteLine(flavors[key]);
                key++;
            }


        }
    }
}
