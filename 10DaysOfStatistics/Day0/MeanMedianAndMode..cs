﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10DaysOfStatistics.Day0
{
    class MeanMedianAndMode
    {
        static double Mean(int[] a)
        {
            int sum = a.Sum();
            return Math.Round((double)sum / a.Length, 1);
        }

        static double Median(int[] a)
        {
            Array.Sort(a);
            int middle = a.Length / 2;
            //int lowMid = (int)Math.Floor(middle);
            if (a.Length % 2 == 0)
            {
                return ((double)(a[middle] + a[middle - 1]) / 2);
            }
            else
            {
                return (double)a[middle];
                
            }
        }

        static int Mode(int[] a)
        {
            int tempCount = 1;
            int maxCount = 0;
            int modeNum = 0;
            for (int i = 0; i < a.Length - 1; i++)
            {
                tempCount = 1;
                for (int j = i + 1; j < a.Length; j++)
                {
                    if (a[i] == a[j])
                    {
                        tempCount++;
                    }
                }
                if (tempCount > maxCount)
                {
                    maxCount = tempCount;
                    modeNum = a[i];
                }
            }
            if (maxCount == 1 || maxCount == 0)
                return a.Min();
            return modeNum;
        }
    }
}
