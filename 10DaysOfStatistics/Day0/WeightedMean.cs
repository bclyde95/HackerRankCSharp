﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10DaysOfStatistics.Day0
{
    class WeightedMean
    {
        static double WeightedMeanFinder(int count, int[] elements, int[] weights)
        {
            int sum = 0;
            for (int i = 0; i < count; i++)
            {
                sum += (elements[i] * weights[i]);
            }
            return (double)sum / weights.Sum();
        }
    }
}
