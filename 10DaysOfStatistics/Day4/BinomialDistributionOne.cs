﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10DaysOfStatistics.Day4
{
    class BinomialDistributionOne
    {
        public static long Factorial(int n)
        {
            if (n == 0 || n == 1)
                return 1;
            else
                return Factorial(n - 1) * n;
        }

        public static double Combination(int n, int x) => (double)Factorial(n) / (Factorial(x) * Factorial(n - x));
        
        public static double BinomialDistribution(int n, int x, double p)
        {
            double q = 1 - p;
            return Combination(n, x) * Math.Pow(p, x) * Math.Pow(q, n - x);
        }

        public static double CDF(int start, int stop, int n, double p, Func<int, int, double, double> f)
        {
            double result = 0.0;

            for (int i = start; i <= stop; i++)
            {
                result += f(n, i, p);
            }
            return result;
        }

        //static void Main(string[] args)
        //{
        //    double[] a = Array.ConvertAll(Console.ReadLine().Split(' '), double.Parse);
        //    double o = a[0] / (a[0] + a[1]);
        //    Console.WriteLine(o);
        //    Console.WriteLine(Math.Round(CDF(3, 6, 6, o, BinomialDistribution), 3));

        //}
    }
}
