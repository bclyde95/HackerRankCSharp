﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10DaysOfStatistics.Day4
{
    class GeometricDistributionTwo
    {
        static double GeometricDistribution(int n, double p)
        {
            double q = 1 - p;
            return Math.Pow(q, n - 1) * p;
        }

        public static double CDF(int start, int stop, double p, Func<int, double, double> f)
        {
            double result = 0.0;

            for (int i = start; i <= stop; i++)
            {
                result += f(i, p);
            }
            return result;
        }

        //static void Main(string[] args)
        //{
        //    Console.WriteLine(CDF(1, 5, 0.7, GeometricDistribution));
        //}
    }
}
