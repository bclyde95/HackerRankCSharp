﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10DaysOfStatistics.Day4
{
    class GeometricDistributionOne
    {
        static double GeometricDistribution(int n, double p)
        {
            double q = 1 - p;
            return Math.Pow(q, n - 1) * p;
        }
        
        //static void Main(string[] args)
        //{
        //    int[] a = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
        //    double p = (double)a[0] / a[1];
        //    int n = int.Parse(Console.ReadLine());
        //    Console.WriteLine(GeometricDistribution(n, p));
        //}
    }
}
