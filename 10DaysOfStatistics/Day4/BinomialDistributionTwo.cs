﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _10DaysOfStatistics.Day4;

namespace _10DaysOfStatistics.Day4
{
    class BinomialDistributionTwo
    {
        public static long Factorial(int n)
        {
            if (n == 0 || n == 1)
                return 1;
            else
                return Factorial(n - 1) * n;
        }

        public static double Combination(int n, int x) => (double)Factorial(n) / (Factorial(x) * Factorial(n - x));

        public static double BinomialDistribution(int n, int x, double p)
        {
            double q = 1 - p;
            return Combination(n, x) * Math.Pow(p, x) * Math.Pow(q, n - x);
        }

        public static double CDF(int start, int stop, int n, double p, Func<int,int,double,double> f)
        {
            double result = 0.0;

            for (int i = start; i <= stop; i++)
            {
                result += f(n, i, p);
            }
            return result;
        }

        //static void Main(string[] args)
        //{
        //    int[] a = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
        //    double p = (double)a[0] / 100;
        //    //No more than 2
        //    Console.WriteLine(Math.Round(CDF(0, 2, a[1], p, BinomialDistribution), 3));
        //    //At least 2
        //    Console.WriteLine(Math.Round(CDF(2, a[1], a[1], p, BinomialDistribution), 3));
        //}
    }
}
