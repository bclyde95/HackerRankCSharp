﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10DaysOfStatistics.Day1
{
    class InterquartileRange
    {
        static int[] SArrayConstructor(int count, int[] elements, int[] frequency)
        {
            int frequencySum = frequency.Sum();
            List<int> temp = new List<int>();
            for (int i = 0; i < count; i++)
            {
                for (int j = 0; j < frequency[i]; j++)
                {
                    temp.Add(elements[i]);
                }
            }
            return temp.ToArray();
        }

        static double Median(int n, int[] a)
        {
            Array.Sort(a);
            int middle = a.Length / 2;
            //int lowMid = (int)Math.Floor(middle);
            if (n % 2 == 0)
            {
                return ((double)(a[middle] + a[middle - 1]) / 2);
            }
            else
            {
                return (double)a[middle];
            }
        }

        static double InterQuartileRange(int count, int[] data)
        {
            data = data.OrderBy(i => i).ToArray();
            double qOne = 0;
            double qTwo = 0;
            double qThree = 0;
            int[] upperHalf = new int[count / 2];
            int[] lowerHalf = new int[count / 2];

            if (count % 2 == 0)
            {
                qTwo = Median(count, data);
                upperHalf = data.Take(count / 2).ToArray();
                qThree = Median(count / 2, upperHalf);
                lowerHalf = data.Skip(count / 2).ToArray();
                qOne = Median(count / 2, lowerHalf);
            }
            else if (count % 2 != 0)
            {
                qTwo = Median(count, data);
                data = data.Where((source, index) => index != (count / 2)).ToArray();
                upperHalf = data.Take(count / 2).ToArray();
                qThree = Median(count / 2, upperHalf);
                lowerHalf = data.Skip(count / 2).ToArray();
                qOne = Median(count / 2, lowerHalf);
            }
            return qOne - qThree;
        }
    }
}
