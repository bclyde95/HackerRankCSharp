﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10DaysOfStatistics.Day1
{
    class StandardDeviation
    {

        //Variance - Average squared distance to the mean (Math.Pow(Sum(i in x, Mean(x)),2) / x.Count)
        //Standard Deviation - (The actual average distance to the mean) Square root of the variance (Math.Sqrt((Math.Pow(Sum(i in x, Mean(x)),2) / x.Count)))
        static double Mean(int[] a)
        {
            int sum = a.Sum();
            return Math.Round((double)sum / a.Length, 1);
        }

        static double StandardDeviationFinder(int count, int[] a)
        {
            double mean = Mean(a);
            double variance = a.Sum(i => Math.Pow(i - mean, 2)) / count;
            return Math.Sqrt(variance); 
        }
    }
}
