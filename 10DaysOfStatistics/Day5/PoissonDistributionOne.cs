﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10DaysOfStatistics.Day5
{
    class PoissonDistributionOne
    {
        static long Factorial(int n)
        {
            if (n == 0 || n == 1)
                return 1;
            else
                return Factorial(n - 1) * n;
        }

        static double PoissonDistribution (int k, double a)
        {
            double aNeg = a - (a * 2);
            return (Math.Pow(a, k) * Math.Pow(Math.E, aNeg)) / Factorial(k);
        }

        public static double CDF(int start, int stop, double a, Func<int, double, double> f)
        {
            double result = 0.0;

            for (int i = start; i <= stop; i++)
            {
                result += f(i, a);
            }
            return result;
        }

        //static void Main(string[] args)
        //{
        //    //Console.WriteLine("{0}", Math.Round(PoissonDistribution(3, 2), 3));
        //    //Console.WriteLine(Math.Round(CDF(0, 3, 5, PoissonDistribution), 3));
        //    Console.WriteLine(Math.Round(PoissonDistribution(5, 2.5),3));
        //}
    }
}
