﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10DaysOfStatistics.Day7
{
    class PearsonCorrelationCoefficientOne
    {
        static double Mean(double[] a)
        {
            double sum = a.Sum();
            return Math.Round(sum / a.Length, 1);
        }

        static double Covariance (double[] x, double[] y, double n)
        {
            double xMean = Mean(x);
            double yMean = Mean(y);
            double sum = 0;
            for (int i = 0; i < n; i++)
            {
                sum += (x[i] - xMean) * (y[i] - yMean);
            }

            return (1 / n) * sum;
        }

        static double StandardDeviation(double count, double[] a)
        {
            double mean = Mean(a);
            double variance = a.Sum(i => Math.Pow(i - mean, 2)) / count;
            return Math.Sqrt(variance);
        }

        static double PearsonCorrelationCoefficient(double[] x, double[] y, double n)
        {
            double sDevX = StandardDeviation(x.Length,x);
            double sDevY = StandardDeviation(y.Length, y);
            return Covariance(x, y, n) / (sDevX * sDevY);
        }

        //static void Main(string[] args)
        //{
        //    double n = double.Parse(Console.ReadLine());
        //    double[] a = Array.ConvertAll(Console.ReadLine().Split(' '), double.Parse);
        //    double[] b = Array.ConvertAll(Console.ReadLine().Split(' '), double.Parse);

        //    Console.WriteLine(PearsonCorrelationCoefficient(a, b, n));
        //    Console.ReadLine();
        //}
    }
}
