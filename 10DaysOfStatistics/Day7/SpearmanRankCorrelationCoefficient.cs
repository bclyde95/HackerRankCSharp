﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10DaysOfStatistics.Day7
{
    class SpearmanRankCorrelationCoefficient
    {
        static double Mean(double[] a)
        {
            double sum = a.Sum();
            return Math.Round(sum / a.Length, 1);
        }

        static double Covariance(double[] x, double[] y, double n)
        {
            double xMean = Mean(x);
            double yMean = Mean(y);
            double sum = 0;
            for (int i = 0; i < n; i++)
            {
                sum += (x[i] - xMean) * (y[i] - yMean);
            }

            return (1 / n) * sum;
        }

        static double StandardDeviation(double count, double[] a)
        {
            double mean = Mean(a);
            double variance = a.Sum(i => Math.Pow(i - mean, 2)) / count;
            return Math.Sqrt(variance);
        }

        static double PearsonCorrelationCoefficient(double[] x, double[] y, double n)
        {
            double sDevX = StandardDeviation(x.Length, x);
            double sDevY = StandardDeviation(y.Length, y);
            return Covariance(x, y, n) / (sDevX * sDevY);
        }

        static double SumOfIOfDToThePowerOfTwo(double[] x, double[] y, double n)
        {
            double sum = 0;
            double[] rankX = Rank(x);
            double[] rankY = Rank(y);
            for (int i = 0; i < n; i++)
                sum +=  Math.Pow(rankX[i] - rankY[i], 2);

            return sum;
        }

        static double[] Rank(double[] a)
        {
            List<double> rank = new List<double>();
            foreach (var item in a.Select((x, i) => new { OldIndex = i, Value = x, NewIndex = -1 })
                .OrderBy(x => x.Value).Select((x, i) => new { OldIndex = x.OldIndex, Value = x.Value, NewIndex = i + 1 })
                .OrderBy(x => x.OldIndex))
                rank.Add(item.NewIndex);
            return rank.ToArray();
        }

        static double SRCCNoDuplicates (double[] x, double[] y, double n) =>
            1 - ((6 * SumOfIOfDToThePowerOfTwo(x, y, n)) / (n * (Math.Pow(n, 2) - 1)));
        
        static double SRCC (double[] x, double[] y, double n) =>
            PearsonCorrelationCoefficient(Rank(x), Rank(y), n);

        //static void Main(string[] args)
        //{
        //    double n = 10;
        //    double[] x = { 10, 9.8, 8, 7.8, 7.7, 1.7, 6, 5, 1.4, 2 };
        //    double[] y = { 200, 44, 32, 24, 22, 17, 15, 12, 8, 4 };
        //    Console.WriteLine(SumOfIOfDToThePowerOfTwo(x, y, n));
        //    Console.WriteLine(SRCC(x, y, n));
        //    Console.WriteLine(SRCCNoDuplicates(x, y, n));
        //    Console.ReadLine();
        //}
    }
}
