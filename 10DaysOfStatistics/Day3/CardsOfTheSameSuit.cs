﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace _10DaysOfStatistics.Day3
{
    class CardsOfTheSameSuit
    {
        public long Factorial(int n)
        {
            if (n == 0 || n == 1)
                return 1;
            else
                return Factorial(n - 1) * n;
        }
        

        // Finding patterns in order to help count the number of favorable events. Permutations(ordered), Combinations(unordered)

        //Permutations - An ordered set of 'r' objects from a set, 'A', of 'n' objects (where 0 < r <= n). "'r'-element permutation of 'A'"
        //factorial(n) / factorial(n - r)

         public double Permutation(int n, int r) => Factorial(n) / Factorial(n - r);

        //Combinations - An unordered set of 'r' objects from a set, 'A', of 'n' objects (where r <= n). "'r'-element combination of 'A'"
        //Combination can be found by dividing the permutation by 'r!', but also as:
        //factorial(n) / (factorial(r) * factorial(n - r))

        public double Combination(int n, int r) => Factorial(n) / (Factorial(r) * Factorial(n - r));
        

        //Problem
        //(13/52) * (12/51) = 3/51. 4*3 = 12/51
    }
}
