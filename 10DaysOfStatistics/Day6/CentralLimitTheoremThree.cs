﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10DaysOfStatistics.Day6
{
    class CentralLimitTheoremThree
    {
        static double ZScore(double sDev, double mean, double x) => (x - mean) / sDev;

        static double MarginOfError(double sDev, double n, double z) => z * (sDev / Math.Sqrt(n));
        

        //static void Main (string[] args)
        //{
        //    double n = double.Parse(Console.ReadLine());
        //    double mean = double.Parse(Console.ReadLine());
        //    double sDev = double.Parse(Console.ReadLine());
        //    double percentage = double.Parse(Console.ReadLine());
        //    double z = double.Parse(Console.ReadLine());

        //    Console.WriteLine();

        //    //Confidence Interval
        //    Console.WriteLine("{0} + or - {1}", mean, MarginOfError(sDev,n,z));

        //    //LowerBound
        //    Console.WriteLine(mean - MarginOfError(sDev, n, z));

        //    //UpperBound
        //    Console.WriteLine(mean + MarginOfError(sDev, n, z));
        //}
    }
}
