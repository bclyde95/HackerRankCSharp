﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10DaysOfStatistics.Day6
{
    class CentralLimitTheoremOne
    {
        static double SND(double x) => (Math.Pow(Math.E, -1 * (Math.Pow(x, 2) / 2))) / Math.Sqrt(2 * Math.PI);

        static double NormalDistribution(double mean, double sDev, double x) => (1 / sDev) * SND((x - mean) / sDev);

        static double Erf(double z)
        {
            //Constants
            double a1 = 0.254829592;
            double a2 = -0.284496736;
            double a3 = 1.421413741;
            double a4 = -1.453152027;
            double a5 = 1.061405429;
            double p = 0.3275911;

            // Sign of x
            int sign = 1;
            if (z < 0)
                sign = -1;
            z = Math.Abs(z);

            //Erf Formula
            double t = 1.0 / (1.0 + p * z);
            double y = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * Math.Exp(-z * z);

            return sign * y;
        }

        static double CumulativeProbability(double sDev, double mean, double x)
        {
            double z = (x - mean) / (sDev * Math.Sqrt(2));
            return ((double)1 / 2) * (1 + Erf(z));
        }

        //static void Main(string[] args)
        //{
        //    double max = double.Parse(Console.ReadLine());
        //    double boxes = double.Parse(Console.ReadLine());
        //    double mean = double.Parse(Console.ReadLine());
        //    double sDev = double.Parse(Console.ReadLine());

        //    Console.WriteLine(CumulativeProbability(sDev * Math.Sqrt(boxes), mean * boxes, max));
        //}
    }
}
