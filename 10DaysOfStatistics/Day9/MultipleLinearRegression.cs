﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord;
using Accord.Statistics;


namespace _10DaysOfStatistics.Day9
{
    class MultipleLinearRegression
    {
        static double Mean(double[] a)
        {
            double sum = a.Sum();
            return sum / a.Length;
        }

        static double Covariance(double[] x, double[] y, double n)
        {
            double xMean = Mean(x);
            double yMean = Mean(y);
            double sum = 0;
            for (int i = 0; i < n; i++)
            {
                sum += (x[i] - xMean) * (y[i] - yMean);
            }

            return (1 / n) * sum;
        }

        static double StandardDeviation(double count, double[] a)
        {
            double mean = Mean(a);
            double variance = a.Sum(i => Math.Pow(i - mean, 2)) / count;
            return Math.Sqrt(variance);
        }

        static double PearsonCorrelationCoefficient(double[] x, double[] y, double n)
        {
            double sDevX = StandardDeviation(x.Length, x);
            double sDevY = StandardDeviation(y.Length, y);
            return Covariance(x, y, n) / (sDevX * sDevY);
        }

        static double LSRL(double[] x, double[] y, double n, double s)
        {
            double b = PearsonCorrelationCoefficient(x, y, n) * (StandardDeviation(n, y) / StandardDeviation(n, x));
            double a = Mean(y) - b * Mean(x);
            return a + b * s;
        }

        static void Main(string[] args)
        {
            
        }
    }
}
